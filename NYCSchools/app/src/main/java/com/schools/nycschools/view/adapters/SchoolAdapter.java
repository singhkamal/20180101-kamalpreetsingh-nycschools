package com.schools.nycschools.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.schools.nycschools.databinding.SchoolViewHolderBinding;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.view.viewHolders.SchoolSelectedObserver;
import com.schools.nycschools.view.viewHolders.SchoolViewHolder;

import java.util.List;

/**
 * Adapter for all the schools
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolViewHolder> {

    private List<School> schools = null;

    private SchoolSelectedObserver schoolSelectedObserver;

    public SchoolAdapter(List<School> schools, SchoolSelectedObserver schoolSelectedObserver) {
        this.schools = schools;
        this.schoolSelectedObserver = schoolSelectedObserver;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SchoolViewHolder(SchoolViewHolderBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        holder.bind(schools.get(position), schoolSelectedObserver);
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }
}
