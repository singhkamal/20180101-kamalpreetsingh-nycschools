package com.schools.nycschools;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.schools.nycschools.core.network.ConnectivityRepo;
import com.schools.nycschools.core.network.ConnectivityRepoImpl;
import com.schools.nycschools.middleware.repository.SchoolRepository;
import com.schools.nycschools.middleware.repository.SchoolRepositoryImpl;
import com.schools.nycschools.middleware.viewmodels.SchoolsAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger enabled dependency factory
 *
 */
@Module
public class DependencyFactory {
    private Application application;

    public DependencyFactory(Application application) {
        this.application = application;
    }

    @Provides
    Context getContext() {
        return application.getApplicationContext();
    }


    @Provides
    @Singleton
    ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    ConnectivityRepo getConnectivityRepo(Context context, ConnectivityManager connectivityManager) {
        return new ConnectivityRepoImpl(context, connectivityManager);
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.HEADERS : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    SchoolsAPI getSchoolsAPI(Retrofit retrofit) {
        return retrofit.create(SchoolsAPI.class);
    }

    @Provides
    @Singleton
    SchoolRepository getSchoolRepository(ConnectivityRepo connectivityRepo, SchoolsAPI schoolsAPI) {
        return new SchoolRepositoryImpl(connectivityRepo, schoolsAPI);
    }
}
