package com.schools.nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.schools.nycschools.Logger;
import com.schools.nycschools.databinding.FragmentLandingBinding;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.viewmodels.ConnectivityViewModel;
import com.schools.nycschools.middleware.viewmodels.SchoolViewModel;
import com.schools.nycschools.utils.EspressoIdling;
import com.schools.nycschools.view.adapters.SchoolAdapter;
import com.schools.nycschools.view.viewHolders.SchoolSelectedObserver;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLanding extends Fragment implements SchoolSelectedObserver {

    private FragmentLandingBinding binding;

    private ConnectivityViewModel connectivityViewModel;

    private SchoolViewModel schoolViewModel;

    public FragmentLanding() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLandingBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        schoolViewModel = new ViewModelProvider(this, new SchoolViewModel.Factory()).get(SchoolViewModel.class);

        connectivityViewModel = new ViewModelProvider(this, new ConnectivityViewModel.Factory()).get(ConnectivityViewModel.class);

        /**
         * If user is not connected to internet, fallback to offline view
         */
        connectivityViewModel.isConnected().observe(getViewLifecycleOwner(), connected -> {
            Logger.d("FragmentLanding connected: " + connected);
            if (!connected) {
                Navigation.findNavController(requireView()).navigate(FragmentLandingDirections.actionFragmentLandingToFragmentOffline());
            }
        });

        /**
            EspressoIdling.INSTANCE.increment(); is used only to assist the espresso testing.
            For more information on this, please check androidTest FragmentLandingTest and {@link EspressoIdling}
         */
        EspressoIdling.INSTANCE.increment();

        schoolViewModel.getAvailableSchools().observe(getViewLifecycleOwner(), schools -> {
            if (schools != null) {
                Logger.d("Schools retrieved: " + schools.size());
                binding.setErrorFetchingSchools(false);
                binding.setSchoolsLoaded(true);

                binding.schoolList.setAdapter(new SchoolAdapter(schools, this));

            } else {
                binding.setErrorFetchingSchools(true);
                binding.setSchoolsLoaded(false);
            }

            EspressoIdling.INSTANCE.decrement();
        });
    }

    /**
     * Called when user clicks an items in the school list. For more information check {@link SchoolAdapter}
     * @param school
     */
    @Override
    public void onSchoolSelected(School school) {
        Logger.d("School selected: " + school.getSchoolName());
        Navigation.findNavController(requireView()).navigate(FragmentLandingDirections.actionFragmentLandingToFragmentSchoolDetail(school));
    }
}
