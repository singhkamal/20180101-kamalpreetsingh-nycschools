package com.schools.nycschools.middleware.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.schools.nycschools.DependencyProvider;
import com.schools.nycschools.core.network.ConnectivityRepo;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.model.SchoolSat;
import com.schools.nycschools.middleware.repository.SchoolRepository;

import java.util.List;

import javax.inject.Inject;

/**
 * Viewmodel wrapping {@link SchoolRepository}
 */
public class SchoolViewModel extends ViewModel implements SchoolRepository {

    /**
     * Factory to assist unit testing
     */
    public static final class Factory implements ViewModelProvider.Factory {

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new SchoolViewModel(DependencyProvider.INSTANCE);
        }
    }

    @Inject
    SchoolRepository schoolRepository;

    @Inject
    ConnectivityRepo connectivityRepo;

    public SchoolViewModel(DependencyProvider dependencyProvider) {
        dependencyProvider.getResolver().inject(this);
    }

    /**
     * Switch Map ensures that API is called as soon as connection becomes available
     * @return live data of list of schools or null if there is an error
     */
    @Override
    public LiveData<List<School>> getAvailableSchools() {
        return Transformations.switchMap(connectivityRepo.isConnected(), connected -> {
            if (connected) {
                return schoolRepository.getAvailableSchools();
            }
            return new MutableLiveData<>();
        });
    }

    /**
     * Switch Map ensures that API is called as soon as connection becomes available
     * @return live data of school sat or null if there is an error
     */
    @Override
    public LiveData<SchoolSat> getSchoolSat(String schoolDbn) {
        return Transformations.switchMap(connectivityRepo.isConnected(), connected -> {
            if (connected) {
                return schoolRepository.getSchoolSat(schoolDbn);
            }
            return new MutableLiveData<>();
        });
    }
}
