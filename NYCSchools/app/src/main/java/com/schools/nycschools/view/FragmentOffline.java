package com.schools.nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.schools.nycschools.databinding.FragmentOfflineBinding;
import com.schools.nycschools.middleware.viewmodels.ConnectivityViewModel;

/**
 * An offline ui fragment
 */
public class FragmentOffline extends Fragment {

    FragmentOfflineBinding bindings;

    public FragmentOffline() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bindings = FragmentOfflineBinding.inflate(inflater, container, false);
        return bindings.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ConnectivityViewModel connectivityViewModel = new ViewModelProvider(this, new ConnectivityViewModel.Factory()).get(ConnectivityViewModel.class);
        /**
         * If we are connected, go back to home
         */
        connectivityViewModel.isConnected().observe(getViewLifecycleOwner(), connected -> {
            if (connected) {
                Navigation.findNavController(requireView()).navigate(FragmentOfflineDirections.actionGlobalLandingFragment());
            }
        });
    }
}
