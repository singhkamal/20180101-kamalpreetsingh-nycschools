package com.schools.nycschools;

import android.app.Application;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new DependencyProvider(this);
    }
}
