package com.schools.nycschools.middleware.repository;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.schools.nycschools.BuildConfig;
import com.schools.nycschools.DummyResponse;
import com.schools.nycschools.core.network.ConnectivityRepo;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.model.SchoolSat;
import com.schools.nycschools.middleware.viewmodels.SchoolsAPI;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test case for testing the contracts of SchoolRepository
 */
@RunWith(MockitoJUnitRunner.class)
public class SchoolRepositoryImplTest {

    @Mock
    ConnectivityRepo connectivityRepo;

    @Rule
    public InstantTaskExecutorRule instantExecutor = new InstantTaskExecutorRule();

    @Before
    public void setup() {

    }

    @After
    public void cleanup() {

    }

    /**
     * Test whether School Repo returns a list of schools if user is offline
     * Expected behavior: Repo should not return any list. Null should be returned instead
     */
    @Test
    public void test_offline_getAvailableSchools() {

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(false));

        SchoolRepository schoolRepository = new SchoolRepositoryImpl(connectivityRepo, mock(SchoolsAPI.class));

        LiveData<List<School>> result = schoolRepository.getAvailableSchools();

        assertNull(result.getValue());
    }

    /**
     * Test whether School Repo returns a SAT score for a school if user is offline
     * Expected behavior: Repo should not return any data. Null should be returned instead
     */
    @Test
    public void test_offline_getSchoolSat() {

        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(false));

        SchoolRepository schoolRepository = new SchoolRepositoryImpl(connectivityRepo, mock(SchoolsAPI.class));

        LiveData<SchoolSat> result = schoolRepository.getSchoolSat("abc");

        assertNull(result.getValue());
    }

    /**
     * School repo should return a list of school if user is online
     *
     * Mockito is used to Mock various dependencies like ConnectivityRepo,
     * SchoolsAPI
     * @throws IOException
     */
    @Test
    public void test_online_getAvailableSchools() throws IOException {

        final CountDownLatch latch = new CountDownLatch(1);

        final boolean[] result = {false};

        // this ensures that connectivity repo is returning "true" during mocking
        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(true));

        // to avoid actual network call, I am mocking the response here. This will ensure that
        // test case keeps running on devices/jenkins test automation servers
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient.Builder()
                        .addInterceptor(DummyResponse.getSchoolsResponseInterceptor())
                        .build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();

        SchoolsAPI api  = retrofit.create(SchoolsAPI.class);

        SchoolRepository schoolRepository = new SchoolRepositoryImpl(connectivityRepo, api);

        // We are not mocking the school repo, but it's dependencies.
        // having a countdown latch will help in exercising the entire look of live data
        schoolRepository.getAvailableSchools().observeForever(schools -> {
            result[0] = schools.size() == 2;
            latch.countDown();
        });

        try {
            latch.await(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // check if repo has returned the expected answer
        assertTrue(result[0]);
    }


    /**
     * School repo should return a School SAT score if user is online
     *
     * Mockito is used to Mock various dependencies like ConnectivityRepo,
     * SchoolsAPI
     * @throws IOException
     */
    @Test
    public void test_online_getSchoolSat() throws IOException {

        final CountDownLatch latch = new CountDownLatch(1);

        final boolean[] result = {false};

        // this ensures that connectivity repo is returning "true" during mocking
        when(connectivityRepo.isConnected()).thenReturn(new MutableLiveData<>(true));

        // to avoid actual network call, I am mocking the response here. This will ensure that
        // test case keeps running on devices/jenkins test automation servers
        Retrofit retrofit = new Retrofit.Builder()
                .client(new OkHttpClient.Builder()
                        .addInterceptor(DummyResponse.getSchoolSatResponseInterceptor())
                        .build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();

        SchoolsAPI api  = retrofit.create(SchoolsAPI.class);

        SchoolRepository schoolRepository = new SchoolRepositoryImpl(connectivityRepo, api);

        // We are not mocking the school repo, but it's dependencies.
        // having a countdown latch will help in exercising the entire look of live data
        schoolRepository.getSchoolSat("21K728").observeForever(schoolSat -> {
            result[0] = schoolSat.getDbn().equals("21K728") & schoolSat.getNumOfSatTestTakers().equals("10");
            latch.countDown();
        });

        try {
            latch.await(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // check if repo has returned the expected answer
        assertTrue(result[0]);
    }
}