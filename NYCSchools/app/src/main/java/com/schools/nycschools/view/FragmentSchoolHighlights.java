package com.schools.nycschools.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schools.nycschools.R;
import com.schools.nycschools.databinding.FragmentSchoolHighlightsBinding;
import com.schools.nycschools.middleware.model.School;

import static com.schools.nycschools.view.Constants.EXTRA_SCHOOL;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSchoolHighlights extends Fragment {

    private FragmentSchoolHighlightsBinding binding;

    public FragmentSchoolHighlights() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolHighlightsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Shows various highlights of the school. {@link School} for more details
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        School school = getArguments().getParcelable(EXTRA_SCHOOL);

        binding.setSchool(school);
    }
}
