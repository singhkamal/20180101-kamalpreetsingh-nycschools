package com.schools.nycschools;

import com.schools.nycschools.middleware.viewmodels.ConnectivityViewModel;
import com.schools.nycschools.middleware.viewmodels.SchoolViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Dagger dependency resolver
 */
@Singleton
@Component(modules = {DependencyFactory.class})
public interface DependencyResolver {

    void inject(ConnectivityViewModel connectivityViewModel);

    void inject(SchoolViewModel schoolViewModel);
}
