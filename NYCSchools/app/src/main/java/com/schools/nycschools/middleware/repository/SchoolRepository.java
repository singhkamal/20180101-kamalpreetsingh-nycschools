package com.schools.nycschools.middleware.repository;

import androidx.lifecycle.LiveData;

import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.model.SchoolSat;

import java.util.List;

/**
 * Basic application school repository
 */
public interface SchoolRepository {

    /**
     * Returns the list of all available schools
     * @return
     */
    LiveData<List<School>> getAvailableSchools();

    /**
     * Returns a school SAT score
     * @param schoolDbn
     * @return
     */
    LiveData<SchoolSat> getSchoolSat(String schoolDbn);
}
