package com.schools.nycschools.utils;

import androidx.test.espresso.idling.CountingIdlingResource;

/**
 * This may sound bad, but Espresso does not wait for network calls to finish.
 * To make espresso wait till the UI loads up data from network, we need to provide a idling
 * resource that espresso can wait on.
 *
 * This class implements a simple counter that blocks espresso if the count is > 0 and when count == 0
 * informs espresso to resume with other instructions
 */
public enum EspressoIdling {
    INSTANCE;

    private static final String RESOURCE = "GLOBAL";

    public final CountingIdlingResource countingIdlingResource = new CountingIdlingResource(RESOURCE);

    public void increment() {
        countingIdlingResource.increment();
    }

    public void decrement() {
        if (!countingIdlingResource.isIdleNow()) {
            countingIdlingResource.decrement();
        }
    }
}
