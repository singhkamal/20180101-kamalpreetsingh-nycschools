package com.schools.nycschools.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schools.nycschools.Logger;
import com.schools.nycschools.R;
import com.schools.nycschools.databinding.FragmentSchoolDetailBinding;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.view.adapters.SchoolDetailAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSchoolDetail extends Fragment {

    private FragmentSchoolDetailBinding binding;

    private School school;

    public FragmentSchoolDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.goBack.setOnClickListener((v) -> Navigation.findNavController(requireView()).navigateUp());

        school = FragmentSchoolDetailArgs.fromBundle(requireArguments()).getSchool();

        binding.setSchool(school);

        /**
         * Launches an intent to dial a school phone.
         */
        binding.schoolPhone.setOnClickListener((v) -> {
            if (!TextUtils.isEmpty(school.getPhoneNumber())) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", school.getPhoneNumber(), null));
                if (requireActivity().getPackageManager().resolveActivity(intent, 0) != null) {
                    startActivity(intent);
                }
            }
        });

        /**
         * Launches an intent that shows the school on maps if google maps is available
         */
        binding.schoolLocation.setOnClickListener((v) -> {
            Uri gmmIntentUri = Uri.parse(String.format("geo:%s,%s?q=%s", school.getLatitude(), school.getLongitude(), school.getLocation()));
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");

            if (requireActivity().getPackageManager().resolveActivity(mapIntent, 0) != null) {
                startActivity(mapIntent);
            }
        });

        /**
         * Launches an intent that shows the school website
         */
        binding.schoolWebsite.setOnClickListener((v) -> {

            String website = school.getWebsite();

            if (!TextUtils.isEmpty(website)) {
                if (!website.startsWith("http://") || !website.startsWith("https://")) {
                    website = "http://" + website;

                    Intent websiteIntent = new Intent(Intent.ACTION_VIEW);
                    websiteIntent.setData(Uri.parse(website));

                    if (requireActivity().getPackageManager().resolveActivity(websiteIntent, 0) != null) {
                        startActivity(websiteIntent);
                    }
                }
            }
        });

        /**
         * Different tabs that shows
         * School SAT score
         * School highlights
         * School bus directions
         */
        binding.pager.setAdapter(new SchoolDetailAdapter(requireContext(), getChildFragmentManager(), school));
        binding.tabBar.setupWithViewPager(binding.pager);
    }
}
