package com.schools.nycschools;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.ResponseBody;

/**
 * I added this class for testing as the API is throttled.
 * This class is used in junit test code.
 */
public class DummyResponse {

    public static Interceptor getSchoolsResponseInterceptor() {
        return chain -> chain.proceed(chain.request())
                .newBuilder()
                .code(200)
                .protocol(Protocol.HTTP_2)
                .message(DummyResponse.SCHOOLS_RESPONSE)
                .body(ResponseBody.create(MediaType.parse("application/json"),
                        DummyResponse.SCHOOLS_RESPONSE.getBytes()))
                .addHeader("content-type", "application/json")
                .build();
    }

    public static Interceptor getSchoolSatResponseInterceptor() {
        return chain -> chain.proceed(chain.request())
                .newBuilder()
                .code(200)
                .protocol(Protocol.HTTP_2)
                .message(DummyResponse.SAT_RESPONSE)
                .body(ResponseBody.create(MediaType.parse("application/json"),
                        DummyResponse.SAT_RESPONSE.getBytes()))
                .addHeader("content-type", "application/json")
                .build();
    }

    public static final String SCHOOLS_RESPONSE = "[{\n" +
            "\t\"dbn\": \"02M260\",\n" +
            "\t\"school_name\": \"Clinton School Writers & Artists, M.S. 260\",\n" +
            "\t\"boro\": \"M\",\n" +
            "\t\"overview_paragraph\": \"Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.\",\n" +
            "\t\"school_10th_seats\": \"1\",\n" +
            "\t\"academicopportunities1\": \"Free college courses at neighboring universities\",\n" +
            "\t\"academicopportunities2\": \"International Travel, Special Arts Programs, Music, Internships, College Mentoring English Language Learner Programs: English as a New Language\",\n" +
            "\t\"ell_programs\": \"English as a New Language\",\n" +
            "\t\"neighborhood\": \"Chelsea-Union Sq\",\n" +
            "\t\"building_code\": \"M868\",\n" +
            "\t\"location\": \"10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)\",\n" +
            "\t\"phone_number\": \"212-524-4360\",\n" +
            "\t\"fax_number\": \"212-524-4365\",\n" +
            "\t\"school_email\": \"admissions@theclintonschool.net\",\n" +
            "\t\"website\": \"www.theclintonschool.net\",\n" +
            "\t\"subway\": \"1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St\",\n" +
            "\t\"bus\": \"BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9\",\n" +
            "\t\"grades2018\": \"6-11\",\n" +
            "\t\"finalgrades\": \"6-12\",\n" +
            "\t\"total_students\": \"376\",\n" +
            "\t\"extracurricular_activities\": \"CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee\",\n" +
            "\t\"school_sports\": \"Cross Country, Track and Field, Soccer, Flag Football, Basketball\",\n" +
            "\t\"attendance_rate\": \"0.970000029\",\n" +
            "\t\"pct_stu_enough_variety\": \"0.899999976\",\n" +
            "\t\"pct_stu_safe\": \"0.970000029\",\n" +
            "\t\"school_accessibility_description\": \"1\",\n" +
            "\t\"directions1\": \"See theclintonschool.net for more information.\",\n" +
            "\t\"requirement1_1\": \"Course Grades: English (87-100), Math (83-100), Social Studies (90-100), Science (88-100)\",\n" +
            "\t\"requirement2_1\": \"Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)\",\n" +
            "\t\"requirement3_1\": \"Attendance and Punctuality\",\n" +
            "\t\"requirement4_1\": \"Writing Exercise\",\n" +
            "\t\"requirement5_1\": \"Group Interview (On-Site)\",\n" +
            "\t\"offer_rate1\": \"Â—57% of offers went to this group\",\n" +
            "\t\"program1\": \"M.S. 260 Clinton School Writers & Artists\",\n" +
            "\t\"code1\": \"M64A\",\n" +
            "\t\"interest1\": \"Humanities & Interdisciplinary\",\n" +
            "\t\"method1\": \"Screened\",\n" +
            "\t\"seats9ge1\": \"80\",\n" +
            "\t\"grade9gefilledflag1\": \"Y\",\n" +
            "\t\"grade9geapplicants1\": \"1515\",\n" +
            "\t\"seats9swd1\": \"16\",\n" +
            "\t\"grade9swdfilledflag1\": \"Y\",\n" +
            "\t\"grade9swdapplicants1\": \"138\",\n" +
            "\t\"seats101\": \"YesÂ–9\",\n" +
            "\t\"admissionspriority11\": \"Priority to continuing 8th graders\",\n" +
            "\t\"admissionspriority21\": \"Then to Manhattan students or residents\",\n" +
            "\t\"admissionspriority31\": \"Then to New York City residents\",\n" +
            "\t\"grade9geapplicantsperseat1\": \"19\",\n" +
            "\t\"grade9swdapplicantsperseat1\": \"9\",\n" +
            "\t\"primary_address_line_1\": \"10 East 15th Street\",\n" +
            "\t\"city\": \"Manhattan\",\n" +
            "\t\"zip\": \"10003\",\n" +
            "\t\"state_code\": \"NY\",\n" +
            "\t\"latitude\": \"40.73653\",\n" +
            "\t\"longitude\": \"-73.9927\",\n" +
            "\t\"community_board\": \"5\",\n" +
            "\t\"council_district\": \"2\",\n" +
            "\t\"census_tract\": \"52\",\n" +
            "\t\"bin\": \"1089902\",\n" +
            "\t\"bbl\": \"1008420034\",\n" +
            "\t\"nta\": \"Hudson Yards-Chelsea-Flatiron-Union Square                                 \",\n" +
            "\t\"borough\": \"MANHATTAN\"\n" +
            "}, {\n" +
            "\t\"dbn\": \"21K728\",\n" +
            "\t\"school_name\": \"Liberation Diploma Plus High School\",\n" +
            "\t\"boro\": \"K\",\n" +
            "\t\"overview_paragraph\": \"The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.\",\n" +
            "\t\"school_10th_seats\": \"1\",\n" +
            "\t\"academicopportunities1\": \"Learning to Work, Student Council, Advisory Leadership, School Newspaper, Community Service Group, School Leadership Team, Extended Day/PM School, College Now\",\n" +
            "\t\"academicopportunities2\": \"CAMBA, Diploma Plus, Medgar Evers College, Coney Island Genera on Gap, Urban Neighborhood Services, Coney Island Coalition Against Violence, I Love My Life Initiative, New York City Police Department\",\n" +
            "\t\"academicopportunities3\": \"The Learning to Work (LTW) partner for Liberation Diploma Plus High School is CAMBA.\",\n" +
            "\t\"ell_programs\": \"English as a New Language\",\n" +
            "\t\"language_classes\": \"French, Spanish\",\n" +
            "\t\"neighborhood\": \"Seagate-Coney Island\",\n" +
            "\t\"building_code\": \"K728\",\n" +
            "\t\"location\": \"2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)\",\n" +
            "\t\"phone_number\": \"718-946-6812\",\n" +
            "\t\"fax_number\": \"718-946-6825\",\n" +
            "\t\"school_email\": \"scaraway@schools.nyc.gov\",\n" +
            "\t\"website\": \"schools.nyc.gov/schoolportals/21/K728\",\n" +
            "\t\"subway\": \"D, F, N, Q to Coney Island  Â–  S llwell Avenue\",\n" +
            "\t\"bus\": \"B36, B64, B68, B74, B82, X28, X38\",\n" +
            "\t\"grades2018\": \"School is structured on credit needs, not grade level\",\n" +
            "\t\"finalgrades\": \"School is structured on credit needs, not grade level\",\n" +
            "\t\"total_students\": \"206\",\n" +
            "\t\"addtl_info1\": \"The Learning to Work (LTW) program assists students in overcoming obstacles that impede their progress toward a high school diploma. LTW offers academic and student support, career and educational exploration, work preparation, skills development, and internships that prepare students for rewarding employment and educational experiences after graduation. These LTW supports are provided by a community-based organization (CBO) partner.\",\n" +
            "\t\"extracurricular_activities\": \"Advisory Leadership, Student Council, Community Service Leadership, School Leadership Team, A er-School Enrichment, Peer Tutoring, School Newspaper\",\n" +
            "\t\"school_sports\": \"Basketball \",\n" +
            "\t\"attendance_rate\": \"0.550000012\",\n" +
            "\t\"pct_stu_enough_variety\": \"0.899999976\",\n" +
            "\t\"pct_stu_safe\": \"0.959999979\",\n" +
            "\t\"transfer\": \"1\",\n" +
            "\t\"directions1\": \"Students must attend an Open House and personalized intake meeting. To find out about open houses, students should call the school at 718-946-6812 or visit our website.\",\n" +
            "\t\"program1\": \"Liberation Diploma Plus High School\",\n" +
            "\t\"code1\": \"L72A\",\n" +
            "\t\"interest1\": \"Humanities & Interdisciplinary\",\n" +
            "\t\"method1\": \"Limited Unscreened\",\n" +
            "\t\"seats9ge1\": \"N/A\",\n" +
            "\t\"grade9gefilledflag1\": \"N/A\",\n" +
            "\t\"grade9geapplicants1\": \"N/A\",\n" +
            "\t\"seats9swd1\": \"N/A\",\n" +
            "\t\"grade9swdfilledflag1\": \"N/A\",\n" +
            "\t\"grade9swdapplicants1\": \"N/A\",\n" +
            "\t\"seats101\": \"Yes-New\",\n" +
            "\t\"eligibility1\": \"For Current 8th Grade Students Â– Open only to students who are at least 15 1/2 years of age and entering high school for the first time. For Other Students Â– Open only to students who are at least 16 years of age and have attended another high school for at least one year\",\n" +
            "\t\"grade9geapplicantsperseat1\": \"N/A\",\n" +
            "\t\"grade9swdapplicantsperseat1\": \"N/A\",\n" +
            "\t\"primary_address_line_1\": \"2865 West 19th Street\",\n" +
            "\t\"city\": \"Brooklyn\",\n" +
            "\t\"zip\": \"11224\",\n" +
            "\t\"state_code\": \"NY\",\n" +
            "\t\"latitude\": \"40.57698\",\n" +
            "\t\"longitude\": \"-73.9854\",\n" +
            "\t\"community_board\": \"13\",\n" +
            "\t\"council_district\": \"47\",\n" +
            "\t\"census_tract\": \"326\",\n" +
            "\t\"bin\": \"3329331\",\n" +
            "\t\"bbl\": \"3070200039\",\n" +
            "\t\"nta\": \"Seagate-Coney Island                                                       \",\n" +
            "\t\"borough\": \"BROOKLYN \"\n" +
            "}]";

    public static final String SAT_RESPONSE = "\n" +
            "[{\"dbn\":\"21K728\",\"school_name\":\"LIBERATION DIPLOMA PLUS\",\"num_of_sat_test_takers\":\"10\",\"sat_critical_reading_avg_score\":\"411\",\"sat_math_avg_score\":\"369\",\"sat_writing_avg_score\":\"373\"}]\n";
}
