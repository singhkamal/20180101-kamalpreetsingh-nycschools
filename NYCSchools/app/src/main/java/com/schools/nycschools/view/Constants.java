package com.schools.nycschools.view;

/**
 * Constants for NYC app
 */
public class Constants {

    // used for passing a parcelable in bundles
    public static final String EXTRA_SCHOOL = "school";
}
