# NYC Schools Test Assignment

Build using Android Jetpack 

- Navigation via Jetpack Navigation
- Majotity of UI rending done using Data Bindings
- Full MVVM architecture
- Accompanying JUnit and Espresso test cases

<br>
<table class="tg">
  <tr>
    <th class="tg-0lax"><h2>Home Screen</h2></th>
    <th class="tg-0lax"><h2>SAT score Screen</h2</th>
    <th class="tg-0lax"><h2>School Highlights Screen</h2</th>
    <th class="tg-0lax"><h2>Bus Availability Screen</h2</th>
  </tr>
  <tr>
    <td class="tg-0lax"><img src="./uploads/1.png" alt="Smiley face" height="555" width="270"></td>
    <td class="tg-0lax"><img src="./uploads/2.png" alt="Smiley face" height="555" width="270"></td>
    <td class="tg-0lax"><img src="./uploads/3.png" alt="Smiley face" height="555" width="270"></td>
    <td class="tg-0lax"><img src="./uploads/4.png" alt="Smiley face" height="555" width="270"></td>
  </tr>
</table>
