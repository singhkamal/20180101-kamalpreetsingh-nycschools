package com.schools.nycschools.middleware.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.schools.nycschools.core.network.ConnectivityRepo;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.model.SchoolSat;
import com.schools.nycschools.middleware.viewmodels.SchoolsAPI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Implementation for {@link SchoolRepository}
 */
public class SchoolRepositoryImpl implements SchoolRepository, Observer {

    // network connectivity state
    private boolean isConnected = false;

    // list of all schools. Data is returned from here
    private MutableLiveData<List<School>> schoolListLiveData = new MutableLiveData<>();

    // set of school SAT. idea here is once data is downloaded, no need to fetch it again
    private Map<String, MutableLiveData<SchoolSat>> schoolSatMap = new HashMap<>();

    private SchoolsAPI schoolsAPI;

    public SchoolRepositoryImpl(ConnectivityRepo connectivityRepo, SchoolsAPI schoolsAPI) {

        this.schoolsAPI = schoolsAPI;

        if (connectivityRepo instanceof Observable) {
            ((Observable)connectivityRepo).addObserver(this);
        }

        updateConnection(connectivityRepo);
    }

    /**
     * Fetches the list of schools
     * @return
     */
    @Override
    public LiveData<List<School>> getAvailableSchools() {
        if (isConnected) {
            if (schoolListLiveData.getValue() == null) {
                schoolsAPI.getSchools().enqueue(new Callback<List<School>>() {
                    @Override
                    public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                        if (response.isSuccessful()) {
                            schoolListLiveData.postValue(response.body());
                        } else {
                            schoolListLiveData.postValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<School>> call, Throwable t) {
                        schoolListLiveData.postValue(null);
                    }
                });
            }
        }
        return schoolListLiveData;
    }


    /**
     * Fetches school's SAT score
     * @param schoolDbn
     * @return
     */
    @Override
    public LiveData<SchoolSat> getSchoolSat(String schoolDbn) {

        MutableLiveData<SchoolSat> schoolSatLiveData = schoolSatMap.getOrDefault(schoolDbn, new MutableLiveData<>());

        if (isConnected) {
            if (!schoolSatMap.containsKey(schoolDbn)) {
                schoolsAPI.getSatDetails(schoolDbn).enqueue(new Callback<List<SchoolSat>>() {
                    @Override
                    public void onResponse(Call<List<SchoolSat>> call, Response<List<SchoolSat>> response) {

                        MutableLiveData<SchoolSat> schoolSatLiveData = schoolSatMap.getOrDefault(schoolDbn, new MutableLiveData<>());

                        if (response.isSuccessful()) {
                            List<SchoolSat> schoolSat = response.body();
                            if (schoolSat != null && schoolSat.size() > 0) {
                                schoolSatLiveData.postValue(schoolSat.get(0));
                            } else {
                                schoolSatLiveData.postValue(null);
                            }
                        } else {
                            schoolSatLiveData.postValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SchoolSat>> call, Throwable t) {
                        MutableLiveData<SchoolSat> schoolSatLiveData = schoolSatMap.getOrDefault(schoolDbn, new MutableLiveData<>());
                        schoolSatLiveData.postValue(null);
                    }
                });
            }
        }

        // Greedy - update hashmap regardless
        schoolSatMap.put(schoolDbn, schoolSatLiveData);

        return schoolSatLiveData;
    }

    /**
     * Called every time, connectivity repo reports a connection change
     * @param o
     * @param arg - is a boolean. True is connected, false is disconnected
     */
    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ConnectivityRepo) {
            isConnected = (Boolean)arg;
        }
    }

    /**
     * Utility function to update connectivity state at startup
     * @param repo
     */
    private void updateConnection(ConnectivityRepo repo) {
        LiveData<Boolean> connectedLiveData = repo.isConnected();
        if (connectedLiveData != null && connectedLiveData.getValue() != null) {
            isConnected = connectedLiveData.getValue();
        }
    }
}
