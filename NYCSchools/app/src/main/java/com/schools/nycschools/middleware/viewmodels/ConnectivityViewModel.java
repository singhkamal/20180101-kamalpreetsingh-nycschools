package com.schools.nycschools.middleware.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.schools.nycschools.DependencyProvider;
import com.schools.nycschools.core.network.ConnectivityRepo;

import javax.inject.Inject;

/**
 * View model that wraps {@link ConnectivityRepo}
 */
public class ConnectivityViewModel extends ViewModel implements ConnectivityRepo {

    /**
     * Factory to assist unit testing
     */
    public static final class Factory implements ViewModelProvider.Factory {

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new ConnectivityViewModel(DependencyProvider.INSTANCE);
        }
    }

    @Inject
    ConnectivityRepo connectivityRepo;

    public ConnectivityViewModel(DependencyProvider dependencyProvider) {
        dependencyProvider.getResolver().inject(this);
    }


    /**
     * Interface to observe the connectivity status
     * @return live data with possible values of true when connected, false otherwise
     */
    @Override
    public LiveData<Boolean> isConnected() {
        return Transformations.map(connectivityRepo.isConnected(), result -> result);
    }
}
