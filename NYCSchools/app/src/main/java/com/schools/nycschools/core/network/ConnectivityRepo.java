package com.schools.nycschools.core.network;

import androidx.lifecycle.LiveData;

/**
 * Observes the desired network for it's availability.
 * Can be queried for the current status
 */
public interface ConnectivityRepo {

    /**
     * Interface to observe the connectivity status
     * @return live data with possible values of true when connected, false otherwise
     */
    LiveData<Boolean> isConnected();
}
