package com.schools.nycschools.view.viewHolders;

import com.schools.nycschools.middleware.model.School;

/**
 * Click observer supplied by FragmentLanding to the adapter. Actual click logic in done via
 * data binding. Please check fragment_landing.xml and school_view_holder.xml
 */
public interface SchoolSelectedObserver {

    void onSchoolSelected(School school);
}
