package com.schools.nycschools.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schools.nycschools.Logger;
import com.schools.nycschools.R;
import com.schools.nycschools.databinding.FragmentSatScoreBinding;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.viewmodels.SchoolViewModel;

import static com.schools.nycschools.view.Constants.EXTRA_SCHOOL;

/**
 * SAT score fragment
 */
public class FragmentSatScore extends Fragment {

    private FragmentSatScoreBinding binding;

    private SchoolViewModel schoolViewModel;

    private School school;

    public FragmentSatScore() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSatScoreBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        school = getArguments().getParcelable(EXTRA_SCHOOL);
        binding.setSchool(school);

        schoolViewModel = new ViewModelProvider(this, new SchoolViewModel.Factory()).get(SchoolViewModel.class);

        /**
         * Fetches the SAT score for a school
         *
         * Most of the magic happens via data bindings, please check XML file for more detail
         */
        schoolViewModel.getSchoolSat(school.getDbn()).observe(getViewLifecycleOwner(), schoolSat -> {
            binding.setSchoolSat(schoolSat);

            if (schoolSat == null) {
                Logger.e("Unable to get school SAT: " + school.getDbn());
                binding.setErrorFetchingSatScored(true);
            } else {
                Logger.d("School SAT available: " + school.getDbn());
                binding.setErrorFetchingSatScored(false);

                binding.totalSatTakers.setText(String.format(getString(R.string.total_sat_takers), schoolSat.getNumOfSatTestTakers()));
                binding.averageMath.setText(String.format(getString(R.string.math_average), schoolSat.getSatMathAvgScore()));
                binding.averageReading.setText(String.format(getString(R.string.reading_average), schoolSat.getSatCriticalReadingAvgScore()));
                binding.averageWriting.setText(String.format(getString(R.string.writing_average), schoolSat.getSatWritingAvgScore()));
            }
        });
    }
}
