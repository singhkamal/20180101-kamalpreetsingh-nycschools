package com.schools.nycschools.view;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.schools.nycschools.MainActivity;
import com.schools.nycschools.R;
import com.schools.nycschools.utils.EspressoIdling;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
 * Basic Espresso test for UI testing
 */
@RunWith(AndroidJUnit4.class)
public class FragmentLandingTest {

    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityActivityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    /**
     * Espresso unit test cases do not like to wait for async task that are typical in any application
     * Resource idling is a way to inform espresso that a test case cannot end because a resource is busy
     *
     * As soon as the resource becomes idle, the test case can exit.
     */
    @Before
    public void setup() {
        // Idle resource that will make espresso framework to wait for network ops
        IdlingRegistry.getInstance().register(EspressoIdling.INSTANCE.countingIdlingResource);
    }

    @After
    public void cleanUp() {
        // Once a testcase is done, remove the idling resource
        IdlingRegistry.getInstance().unregister(EspressoIdling.INSTANCE.countingIdlingResource);
    }

    /**
     * Basic testcase demonstrating that when application first loads, it tries to load content
     */
    @Test
    public void test_isLoading() {
        onView(withId(R.id.landing)).check(matches(isDisplayed()));
    }

    /**
     * This testcase launches the app, makes an actual network call, performs a click on the recyclerview
     * item and checks if user got navigated to School detail Fragment
     *
     * This testcase uses IdleResource so that it can wait for the network calls to finish
     */
    @Test
    public void test_hasContent() {
        onView(withId(R.id.school_list)).check(matches(isDisplayed()));
        onView(withId(R.id.school_list)).perform(RecyclerViewActions.actionOnItemAtPosition(4, click()));
        onView(withId(R.id.school_name)).check(matches(isDisplayed()));
    }
}