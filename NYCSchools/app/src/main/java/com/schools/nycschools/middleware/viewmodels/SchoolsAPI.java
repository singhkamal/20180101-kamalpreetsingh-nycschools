package com.schools.nycschools.middleware.viewmodels;

import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.middleware.model.SchoolSat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit API interface.
 */
public interface SchoolsAPI {

    /**
     * Get the list of all available schools
     * @return
     */
    @GET("resource/s3k6-pzi2.json")
    Call<List<School>> getSchools();

    /**
     * Get SAT score for a particular school
     * @param dbn - query param returned via {@link SchoolsAPI#getSchools()}
     * @return
     */
    @GET("resource/f9bf-2cp4.json")
    Call<List<SchoolSat>> getSatDetails(@Query("dbn") String dbn);
}
