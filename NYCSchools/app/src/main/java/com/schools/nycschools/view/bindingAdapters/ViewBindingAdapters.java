package com.schools.nycschools.view.bindingAdapters;

import android.view.View;

import androidx.databinding.BindingAdapter;

public class ViewBindingAdapters {

    @BindingAdapter("gone")
    public static void gone(View view, boolean gone) {
        view.setVisibility(gone ? View.GONE : View.VISIBLE);
    }

    @BindingAdapter("visible")
    public static void visible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("invisible")
    public static void invisible(View view, boolean invisible) {
        view.setVisibility(invisible ? View.INVISIBLE : View.VISIBLE);
    }

}
