package com.schools.nycschools;

import android.util.Log;

/**
 * Simple wrapper class for logging
 * Such class can be used to send logs to a file/server etc
 */
public class Logger {

    private static final String TAG = "NYSCHOOLS";

    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message);
        }
    }

    public static void e(String message) {
        Log.e(TAG, message);
    }
}
