package com.schools.nycschools.view.viewHolders;

import androidx.recyclerview.widget.RecyclerView;

import com.schools.nycschools.databinding.SchoolViewHolderBinding;
import com.schools.nycschools.middleware.model.School;

public class SchoolViewHolder extends RecyclerView.ViewHolder {

    SchoolViewHolderBinding binding;

    public SchoolViewHolder(SchoolViewHolderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    /**
     * Location is the school is returned as
     * "location": "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
     * I do not want to show lat long in UI
     * @param school
     * @param schoolSelectedObserver
     */
    public void bind(School school, SchoolSelectedObserver schoolSelectedObserver) {
        school.setLocation(school.getPrimaryAddressLine1() + " " + school.getCity() + " " + school.getZip());
        binding.setSchool(school);
        binding.setSchoolSelecedObserver(schoolSelectedObserver);
    }
}
