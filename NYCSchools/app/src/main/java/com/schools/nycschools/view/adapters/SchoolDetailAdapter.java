package com.schools.nycschools.view.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.schools.nycschools.R;
import com.schools.nycschools.middleware.model.School;
import com.schools.nycschools.view.FragmentSatScore;
import com.schools.nycschools.view.FragmentSchoolBusDirections;
import com.schools.nycschools.view.FragmentSchoolHighlights;

import static com.schools.nycschools.view.Constants.EXTRA_SCHOOL;

/**
 * To assit the tabs on School Detail fragment
 */
public class SchoolDetailAdapter extends FragmentPagerAdapter {

    private Resources resources;

    private School school;

    public SchoolDetailAdapter(Context context, @NonNull FragmentManager fm, School school) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        resources = context.getResources();
        this.school = school;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_SCHOOL, school);

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FragmentSatScore();
                break;
            case 1:
                fragment = new FragmentSchoolHighlights();
                break;
            default:
                fragment = new FragmentSchoolBusDirections();
                break;
        }

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.sat);
            case 1:
                return resources.getString(R.string.highlights);
            default:
                return resources.getString(R.string.directions);
        }
    }
}
